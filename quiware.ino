const unsigned int kAmountLevels = 8;
const unsigned int kBrokenLevels = 2;
const unsigned int kModeDelay= 3000;
// global speed limits
// 255 is stop, 0 is full speed
const unsigned int kSpeedMaximum = 140;
const unsigned int kSpeedMinimum= 175;
// speedFactor
// factor for the range given above
const unsigned int kSpeedFactorSafe = 1;
// constants describing the set
const unsigned int kDelayBeforeSet = 15000;
const unsigned int kMovesPerSet = 7;
const unsigned int kMovesUntilCalibration = 0;
const unsigned int kDelayAfterCalibration = 20000;
const unsigned int kMovementDelay = 3000;
// list of {LeveltimePercent, speedPercent} to model moves
const int kDownMove [] [2] = {{168,1}, {0,0}};
const int kUpMove [] [2] = {{100,100}};

#define p_led_run 13
#define p_limit_mechanical_upper 14
#define p_limit_automatic_upper 15
#define p_limit_automatic_lower 16
#define p_limit_mechanical_lower 17
#define p_button_up 12
#define p_button_down 11
#define p_switch_autostart 10
#define p_button_start 9
#define p_switch_mode 8
#define p_pot_speed A7
#define p_motor_up 4
#define p_motor_down 2
#define p_motor_pwm 3
#define p_switch_emergency 7
#define p_random A0

unsigned long duration_bottom_2_top = 0;
unsigned long level_duration = duration_bottom_2_top / kAmountLevels;
int zone = 0;

void write(int write_input = 0) {
  bool up = 0;
  bool down = 0;
  unsigned int pwm = 255;
  if (!digitalRead(p_switch_emergency)) {
    pwm = map(abs(write_input), 0, 100, kSpeedMinimum, kSpeedMaximum);
    if (write_input < 0 && !digitalRead(p_limit_mechanical_lower) &&
        (!digitalRead(p_switch_mode) || !digitalRead(p_limit_automatic_lower)))
    {
      down = 1;
    } else if (write_input > 0 && !digitalRead(p_limit_mechanical_upper) &&
        (!digitalRead(p_switch_mode) || !digitalRead(p_limit_automatic_upper)))
    {
      up = 1;
    }
  }
  digitalWrite(p_motor_up, up);
  digitalWrite(p_motor_down, down);
  analogWrite(p_motor_pwm, pwm);
}

void moveLevelTop() {
  Serial.print("moving to toplevel...");
  while (!digitalRead(p_limit_automatic_upper)
      && !digitalRead(p_switch_emergency)) {
    write(100);
  }
  write();
  Serial.println(" done!");
}

void moveLevelDown() { 
  unsigned long millis_start = 0;
  for (int i = 0; i < (sizeof(kDownMove) / sizeof(int) / 2); i++) {
    millis_start = millis();
    while ((unsigned long)(millis() - millis_start)
        < (level_duration*kDownMove[i][0]/100)
        && !digitalRead(p_switch_emergency)) {
      write(-1*kDownMove[i][1]);
    }
  }
  write();
}

void moveLevelUp() {
  unsigned long millis_start = 0;
  for (int i = 0; i < (sizeof(kUpMove) / sizeof(int) / 2); i++) {
    millis_start = millis();
    while ((unsigned long)(millis() - millis_start)
        < (level_duration*kUpMove[i][0]/100)
        && !digitalRead(p_switch_emergency)) {
      write(kUpMove[i][1]);
    }
  }
  write();
}

void movement(int move_levels) {
  for (int i = abs(move_levels); i > 0; i--) {
    if (move_levels > 0) {
      moveLevelUp();
    } else if (move_levels < 0) {
      moveLevelDown();
    }
  }
  delay(kMovementDelay);
}

void calibrate() {
  if (duration_bottom_2_top == 0) { // first initial calibration
    Serial.println("initial calibration.");
    while (!digitalRead(p_limit_automatic_lower)
           && !digitalRead(p_switch_emergency)) {
      write(kSpeedFactorSafe * -1);
    }
  } else if (duration_bottom_2_top != 0) { // calibration during moveset
    movement(0 - kAmountLevels);
  }
  if (digitalRead(p_limit_automatic_lower)) {
    Serial.print("measuring...   ");
    unsigned long duration_start = millis();
    moveLevelTop();
    duration_bottom_2_top = (unsigned long)(millis() - duration_start);
    level_duration = duration_bottom_2_top / kAmountLevels;
    Serial.print(duration_bottom_2_top);
    Serial.println(" ms for full travel. calibrated succesfully!");
  }
  write();
}

void driveSet() {
  int moves = 0;
  bool calibrated = 0;
  unsigned long time_set = millis();
  Serial.println("starting set");
  moveLevelTop();
  int current_level = kAmountLevels;
  delay(kDelayBeforeSet); 
  while ((moves < kMovesPerSet
          || (current_level > kAmountLevels - kBrokenLevels
            && current_level < kAmountLevels))
        && digitalRead(p_switch_mode) && !digitalRead(p_switch_emergency)) {
    bool next_direction = 0; // 1 is up, 0 is down
    int how_far = 0;
    if ( (moves >= kMovesUntilCalibration) && calibrated == 0) {
      calibrate();
      calibrated = 1;
      current_level = kAmountLevels;
      delay(kDelayAfterCalibration);
    }
    if (digitalRead(p_limit_automatic_upper)) {
      current_level = kAmountLevels;
      next_direction = 0;
    } else if (digitalRead(p_limit_automatic_lower)) {
      current_level = 0;
      next_direction = 1;
    } else if (current_level >= kAmountLevels - kBrokenLevels) {
      next_direction = 0;
    } else {
      next_direction = random(0, 2);
    }
    if (next_direction == 1) {
      int max_distance = 0;
      max_distance = kAmountLevels - current_level;
      how_far = random(1, max_distance);
      movement(how_far);
      moves += abs(how_far);
      current_level += how_far;
    } else if (next_direction == 0) {
      how_far = random(1, current_level);
      movement(0 - how_far);
      moves += abs(how_far);
      current_level -= how_far;
    }
    Serial.print("set: moved ");
    Serial.print(moves);
    Serial.println(" levels so far");
  }
  moveLevelTop();
  Serial.print("set: finished with ");
  Serial.print(moves);
  Serial.print(" movements. ms elapsed: ");
  Serial.println(millis() - time_set);
  write();
}

void automaticMode() {
  Serial.println("entering automatic mode");
  while (!digitalRead(p_switch_emergency)  && digitalRead(p_switch_mode)) {
    if (!digitalRead(p_button_start) && !digitalRead(p_switch_autostart)) {
      if (duration_bottom_2_top != 0) {
        digitalWrite(p_led_run, HIGH);
        driveSet();
        digitalWrite(p_led_run, LOW);
      } else {
        calibrate();
      }
    }
  }
  write ();
  Serial.println("exiting automatic mode");
}

void outOfBounds() {
  if ((abs(zone) == 2)
      && ((!digitalRead(p_limit_automatic_upper)
          && !digitalRead(p_button_down))
        || (!digitalRead(p_limit_automatic_lower)
          && !digitalRead(p_button_up)))) {
    zone = 0;
  }
  if (digitalRead(p_limit_automatic_upper)) {
    while (!digitalRead(p_button_up) && zone != 1) {
      write ();
      delay(500);
    }
    zone = 1;
    if (!digitalRead(p_button_down)) {
      zone = 2;
    }
  }
  if (digitalRead(p_limit_automatic_lower)) {
    while (!digitalRead(p_button_down) && zone != -1) {
      write ();
      delay(500);
    }
    zone = -1;
    if (!digitalRead(p_button_up)) {
      zone = -2;
    }
  }
}

void manualMode() {
  Serial.println("entering manual mode");
  int write_input = 0;
  unsigned int pot_speed = 0;
  
  while (!digitalRead(p_switch_emergency) && !digitalRead(p_switch_mode)) {
    pot_speed = analogRead(p_pot_speed);
    if (!digitalRead(p_button_up)) {
      write_input = map(pot_speed, 0, 1023, 0, 100);
    } else if (!digitalRead(p_button_down)) {
      write_input = map(pot_speed, 0, 1023, 0, -100);
    } else {
      write_input = 0;
    }
    outOfBounds();
    write(write_input);
  }
  write();
  Serial.println("exiting manual mode");
}

void setup() {
  Serial.begin(9600);
  Serial.println("   ____    __    __ _____  ___     ___  ____   ______    ____ _  ");
  Serial.println("  / __ \\   ) )  ( ((_   _)(  (       )  )(    ) (   __ \\  / ___/  ");
  Serial.println(" / /  \\ \\ ( (    ) ) | |   \\  \\  _  /  / / /\\ \\  ) (__) )( (__    ");
  Serial.println("( (    ) ) ) )  ( (  | |    \\  \\/ \\/  / ( (__) )(    __/  ) __)   ");
  Serial.println("( (  /\\) )( (    ) ) | |     )   _   (   )    (  ) \\ \\  _( (      ");
  Serial.println(" \\ \\_\\ \\/  ) \\__/ ( _| |__   \\  ( )  /  /  /\\  \\( ( \\ \\_))\\ \\___  ");
  Serial.println("  \\___\\ \\_ \\______//_____(    \\_/ \\_/  /__(  )__\\)_) \\__/  \\____\\ ");
  Serial.println("       \\__)                                                       ");
  Serial.println("controlling \"all of a quiver\" by Mona Hatoum");
  // setting up the pins
  pinMode(p_limit_mechanical_upper, INPUT_PULLUP);
  pinMode(p_limit_automatic_upper, INPUT_PULLUP);
  pinMode(p_limit_automatic_lower, INPUT_PULLUP);
  pinMode(p_limit_mechanical_lower, INPUT_PULLUP);
  pinMode(p_button_up, INPUT_PULLUP);
  pinMode(p_button_down, INPUT_PULLUP);
  pinMode(p_button_start, INPUT_PULLUP);
  pinMode(p_switch_autostart, INPUT_PULLUP);
  pinMode(p_switch_emergency, INPUT_PULLUP);
  pinMode(p_switch_mode, INPUT_PULLUP);
  pinMode(p_pot_speed, INPUT);
  pinMode(p_led_run, OUTPUT);
  pinMode(p_motor_up, OUTPUT);
  pinMode(p_motor_down, OUTPUT);
  pinMode(p_motor_pwm, OUTPUT);
  pinMode(LED_BUILTIN, OUTPUT);
  // achieving random movements
  pinMode(p_random, INPUT);
  randomSeed(analogRead(p_random));
}

void loop() {
  write();
  if (!digitalRead(p_switch_emergency)) {
    if (!digitalRead(p_switch_mode)) {
      manualMode();
    } else if (digitalRead(p_switch_mode)) {
      automaticMode();
    }
  } else {
    digitalWrite(LED_BUILTIN, HIGH);
    Serial.println("EMERGENCY STOP!");
  }
  digitalWrite(LED_BUILTIN, LOW);
  write();
  delay(kModeDelay);
}
