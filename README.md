# quiware
An Arduino software to control *all of a quiver*, an installation by Mona Hatoum.

## contributions
technical lead: Jürgen Steger - [STEGER exponatbau](https://exponatbau.de/)  
hardware design: typ_o  

## how to
* clone repository or download quiware.ino manually
* open quiware.ino in an Arduino IDE version 1.8.19  
	  https://downloads.arduino.cc/arduino-1.8.19-windows.zip  
	  https://downloads.arduino.cc/arduino-1.8.19-linuxaarch64.tar.xz  
* select Tools > Boards > Arduino Nano
* select Tools > Processor > ATmega328P (Old Bootloader)
* connect the Arduino to an USB-port
* go to Tools > Port > 
* select the correct port for the Arduino
* select Sketch > Upload
* if no errors occur on the bottom of the screen the controller has been programmed correctly
* to further verify the proper function, click on the magnifier in the top right corner of the IDE.  
		it'll show output whenever the arduino is reset (button on chip)

## License

Code is under the [BSD 2-clause License](LICENSE.txt).
